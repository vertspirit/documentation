INKSCAPE_REPO_DIR:=inkscape.po
WEBSITE_REPO_DIR:=web-i18n.git

# default INKBRANCH to "master"
ifneq ($(MAKECMDGOALS), help)
ifndef INKBRANCH
$(warning 'INKBRANCH' is not set, assuming 'master')
override INKBRANCH := master
endif
endif


.PHONY: all
all: download-po
	./language_statistics.sh \
		--docs-dir ../ \
		--inkscape-dir $(INKSCAPE_REPO_DIR) \
		--website-dir $(WEBSITE_REPO_DIR) \
		--version $(INKBRANCH) > translation_statistics.html


.PHONY: copy
copy:
	mkdir -p ../export-website/devel
	cp translation_statistics.html ../export-website/devel/translations-statistics-$(INKBRANCH).html
	cp statistics.css ../export-website/devel/statistics-$(INKBRANCH).css
	sed -i -e 's/statistics.css/statistics-$(INKBRANCH).css/g' ../export-website/devel/translations-statistics-$(INKBRANCH).html


.PHONY: clean
clean:
	rm -f *.html


# we don't actually need to create any po files, so this is an empty target
po: ;


.PHONY: download-po
download-po: po-inkscape po-website

.PHONY: po-inkscape
po-inkscape:
	# downloading updated Inkscape po directory from CI
	rm -rf $(INKSCAPE_REPO_DIR) translations.zip
	wget --output-document=translations.zip --no-verbose \
		https://gitlab.com/api/v4/projects/3472737/jobs/artifacts/$(INKBRANCH)/download?job=translations
	unzip -q translations.zip -d $(INKSCAPE_REPO_DIR)
	rm translations.zip

.PHONY: po-website
po-website:
ifeq ($(wildcard $(WEBSITE_REPO_DIR)/.git),)
	# initializing a new git repo in $(WEBSITE_REPO_DIR)
	mkdir -p $(WEBSITE_REPO_DIR)
	git clone --depth 1 https://gitlab.com/inkscape/inkscape-web-i18n.git $(WEBSITE_REPO_DIR)
else
	# pulling remote changes from inkscape-web-i18n repo
	cd $(WEBSITE_REPO_DIR) && git pull
endif


.PHONY: help
help:
	@echo "Targets:"
	@echo "   all         - Create translation statistics (.html)."
	@echo "   copy        - Copy generated HTML files to the export directory"
	@echo "   clean       - Remove all generated files"
	@echo
	@echo "   download-po - Download po dirs from 'inkscape' and 'inkscape-web-i18n' git repositories"
	@echo "                 to allow for creation of UI and website translation statistics"


# no-op for CI
version: ;
check-input: ;
check-output: ;
