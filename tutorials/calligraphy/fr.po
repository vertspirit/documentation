#
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Calligraphy\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-10-11 17:29+0200\n"
"PO-Revision-Date: 2020-05-07 19:49+0200\n"
"Last-Translator: Guillaume Audirac <guillaume.audirac@viregul.fr>\n"
"Language-Team: French <fr@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.6\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""
"Matiphas <matiphas@free.fr>, 2005, 2006\n"
"Popolon <popolon@popolon.org>, 2007\n"
"jazzynico <nicoduf@yahoo.fr>, 2014\n"
"Frigory <chironsylvain@orange.fr>, 2016\n"
"gadic <guillaume.audirac@viregul.fr>, 2020"

#. (itstool) path: articleinfo/title
#: tutorial-calligraphy.xml:6
msgid "Calligraphy"
msgstr "Calligraphie"

#. (itstool) path: articleinfo/subtitle
#: tutorial-calligraphy.xml:7
msgid "Tutorial"
msgstr "Didacticiel"

#. (itstool) path: abstract/para
#: tutorial-calligraphy.xml:11
msgid ""
"One of the many great tools available in Inkscape is the Calligraphy tool. "
"This tutorial will help you become acquainted with how that tool works, as "
"well as demonstrate some basic techniques of the art of Calligraphy."
msgstr ""
"L'outil Calligraphie est l'un des nombreux magnifiques outils d'Inkscape. Ce "
"didacticiel vous aidera à découvrir le fonctionnement de cet outil, ainsi "
"que les bases et techniques de l'art de la calligraphie."

#. (itstool) path: abstract/para
#: tutorial-calligraphy.xml:15
#, fuzzy
msgid ""
"Use <keycombo><keycap function=\"control\">Ctrl</keycap><keycap>arrows</"
"keycap></keycombo>, <mousebutton role=\"mouse-wheel\">mouse wheel</"
"mousebutton>, or <mousebutton role=\"middle-button-drag\">middle button "
"drag</mousebutton> to scroll the page down. For basics of object creation, "
"selection, and transformation, see the Basic tutorial in "
"<menuchoice><guimenu>Help</guimenu><guimenuitem>Tutorials</guimenuitem></"
"menuchoice>."
msgstr ""
"Utiliser <keycombo><keycap function=\"control\">Ctrl</keycap><keycap>flèche</"
"keycap></keycombo>, la <mousebutton role=\"mouse-button\">molette de la "
"souris</mousebutton> ou le <mousebutton role=\"middle-button-drag\">bouton "
"du milieu</mousebutton> pour faire défiler la page vers le bas. Pour les "
"bases de création, sélection et transformation d'objet, lisez le didacticiel "
"basique dans <menuchoice><guimenu>Aide</guimenu><guimenuitem>Didacticiels</"
"guimenuitem></menuchoice>."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:23
msgid "History and Styles"
msgstr "Histoire et styles"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:24
msgid ""
"Going by the dictionary definition, <firstterm>calligraphy</firstterm> means "
"“beautiful writing” or “fair or elegant penmanship”. Essentially, "
"calligraphy is the art of making beautiful or elegant handwriting. It may "
"sound intimidating, but with a little practice, anyone can master the basics "
"of this art."
msgstr ""
"Par définition, la <firstterm>calligraphie</firstterm> signifie « belle "
"écriture » ou « faculté d'écrire élégamment ». La calligraphie est "
"essentiellement l'art de former à la main de beaux caractères d'écriture. "
"Cela peut sembler intimidant, mais avec un peu de pratique, n'importe qui "
"peut maîtriser les bases de cet art."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:29
#, fuzzy
msgid ""
"Up until roughly 1440 AD, before the printing press was around, calligraphy "
"was the way books and other publications were made. A scribe had to "
"handwrite every individual copy of every book or publication. Perhaps the "
"most common place where the average person will run across calligraphy today "
"is on wedding invitations. There are three main styles of calligraphy:"
msgstr ""
"Les premières formes de calligraphie apparaissent avec les peintures des "
"hommes des cavernes. Et jusqu'à l'apparition de l'imprimerie, en 1440 "
"environ, les livres et autres publications étaient calligraphiés. Un scribe "
"devait transcrire à la main toute copie de livre ou publication. L'écriture "
"se faisait avec une plume (d'oie) et de l'encre sur des matériaux tels que "
"du parchemin ou du vélin. Les styles de lettrage utilisés au cours des âges "
"incluent le Rustique, la minuscule Caroline, le Blackletter, etc. "
"Aujourd'hui, l'emplacement le plus commun où une personne classique trouvera "
"de la calligraphie est probablement le faire-part d'un mariage."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:32
msgid "There are three main styles of calligraphy:"
msgstr "Il existe trois principales familles de calligraphie :"

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:37
msgid ""
"Western or Roman: The preserved texts are mainly written with a quill and "
"ink onto materials such as parchment or vellum."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:42
msgid ""
"Arabic, including other languages in Arabic script, executed with a reed pen "
"and ink on paper. (This style has produced by far the largest bulk of "
"preserved texts before 1440.)"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:47
#, fuzzy
msgid "Chinese or Oriental, executed with a brush."
msgstr "Chinoise ou orientale"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:52
msgid ""
"The three styles have influenced each other on different occasions. Western "
"lettering styles used throughout the ages include Rustic, Carolingian, "
"Blackletter, etc."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:55
msgid ""
"One great advantage that we have over the scribes of the past is the "
"<guimenuitem>Undo</guimenuitem> command: If you make a mistake, the entire "
"page is not ruined. Inkscape's Calligraphy tool also enables some techniques "
"which would not be possible with a traditional pen-and-ink."
msgstr ""
"Un des grands avantages que nous avons sur les scribes du passé est la "
"commande <guimenuitem>Annuler</guimenuitem>. Si vous faites une erreur, la "
"page entière n'est pas perdue. L'outil Calligraphie d'Inkscape permet aussi "
"des techniques qui ne seraient pas possible avec les plumes et encres "
"traditionnelles."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:62
msgid "Hardware"
msgstr "Matériel"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:63
msgid ""
"You will get the best results if you use a <firstterm>tablet and pen</"
"firstterm> (e.g. Wacom). Thanks to the flexibility of our tool, even those "
"with only a mouse can do some fairly intricate calligraphy, though there "
"will be some difficulty producing fast sweeping strokes."
msgstr ""
"Vous obtiendrez de meilleurs résultats si vous utilisez une "
"<firstterm>tablette graphique avec stylet</firstterm> (ex. : une Wacom). "
"Mais grâce à la flexibilité de notre outil, vous pouvez également obtenir "
"des résultats décents avec une souris, même s'il vous sera plus difficile de "
"produire des tracés rapides réguliers."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:68
msgid ""
"Inkscape is able to utilize <firstterm>pressure sensitivity</firstterm> and "
"<firstterm> tilt sensitivity</firstterm> of a tablet pen that supports these "
"features. The sensitivity functions are disabled by default because they "
"require configuration. Also, keep in mind that calligraphy with a quill or "
"pen with nib are also not very sensitive to pressure, unlike a brush."
msgstr ""
"Inkscape est capable d'utiliser les sensibilités à la <firstterm>pression</"
"firstterm> et à l'<firstterm>inclinaison</firstterm> d'un stylet de tablette "
"qui prend en charge ces fonctions. Les fonctions de sensibilité sont "
"désactivées par défaut parce qu'elles nécessitent de la configuration. "
"Souvenez-vous par ailleurs que la calligraphie avec une plume ou un stylo "
"avec pointe ne sont pas vraiment sensibles à la pression, contrairement à un "
"pinceau."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:74
msgid ""
"If you have a tablet and would like to utilize the sensitivity features, you "
"will need to configure your device. This configuration will only need to be "
"performed once and the settings are saved. To enable this support you must "
"have the tablet plugged in prior to starting Inkscape and then proceed to "
"open the <guimenuitem>Input Devices...</guimenuitem> dialog through the "
"<guimenu>Edit</guimenu> menu. With this dialog open you can choose the "
"preferred device and settings for your tablet pen. Lastly, after choosing "
"those settings, switch to the Calligraphy tool and toggle the toolbar "
"buttons for pressure and tilt. From now on, Inkscape will remember those "
"settings on startup."
msgstr ""
"Si vous avez une tablette et désirez utiliser les fonctions de sensibilité, "
"vous devez configurer votre périphérique. Cette configuration n'a besoin "
"d'être effectuée qu'une seule fois, les réglages seront sauvegardés. Pour en "
"activer le support, vous devez avoir la tablette graphique branchée avant de "
"démarrer Inkscape, puis, ouvrir la boîte de dialogue "
"<guimenuitem>Périphériques de saisie…</guimenuitem>, via le menu "
"<guimenu>Édition</guimenu>. Lorsque cette boîte de dialogue est ouverte, "
"vous pouvez sélectionner le périphérique et les réglages que vous désirez "
"pour le stylet de votre tablette. Enfin, après avoir choisi ces réglages, "
"passez à l'outil Calligraphie et commutez les boutons de la barre d'outils "
"pour la pression et l'inclinaison. Inkscape se souviendra de ces réglages au "
"prochain démarrage."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:83
msgid ""
"The Inkscape calligraphy pen can be sensitive to the <firstterm>velocity</"
"firstterm> of the stroke (see “Thinning” below), so if you are using a "
"mouse, you'll probably want to zero this parameter."
msgstr ""
"Le stylo calligraphique d'Inkscape peut être sensible à la "
"<firstterm>vélocité</firstterm> du tracé (voir « Amincissement » plus bas), "
"donc, si vous utilisez une souris, vous désirerez probablement mettre ce "
"paramètre à zéro."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:89
msgid "Calligraphy Tool Options"
msgstr "Options de l'outil Calligraphie"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:90
msgid ""
"Switch to the Calligraphy tool by pressing <keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>F6</keycap></keycombo>, pressing the "
"<keycap>C</keycap> key, or by clicking on its toolbar button. On the top "
"toolbar, you will notice there are 8 options: <guilabel>Width</guilabel> "
"&amp; <guilabel>Thinning</guilabel>; <guilabel>Angle</guilabel> &amp; "
"<guilabel>Fixation</guilabel>; <guilabel>Caps</guilabel>; <guilabel>Tremor</"
"guilabel>, <guilabel>Wiggle</guilabel> &amp; <guilabel>Mass</guilabel>. "
"There are also two buttons to toggle tablet Pressure and Tilt sensitivity on "
"and off (for drawing tablets)."
msgstr ""
"Passez à l'outil Calligraphie en appuyant sur <keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>F6</keycap></keycombo> ou sur <keycap>C</"
"keycap>, ou encore en cliquant sur son bouton dans la barre d'outils. Vous "
"remarquerez alors, dans la barre d'outils du haut, huit options : "
"<guilabel>largeur</guilabel> &amp; <guilabel>amincissement</guilabel> ; "
"<guilabel>angle</guilabel> &amp; <guilabel>fixité</guilabel> ; "
"<guilabel>terminaisons</guilabel> ; <guilabel>tremblement</guilabel>, "
"<guilabel>agitation</guilabel> &amp; <guilabel>inertie</guilabel>. Il y a "
"également deux autres boutons pour activer ou désactiver la pression de la "
"tablette et la sensibilité à l'inclinaison (pour les tablettes graphiques)."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:101
msgid "Width &amp; Thinning"
msgstr "Largeur et amincissement"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:102
msgid ""
"This pair of options control the <firstterm>width</firstterm> of your pen. "
"The width can vary from 1 to 100 and (by default) is measured in units "
"relative to the size of your editing window, but independent of zoom. This "
"makes sense, because the natural “unit of measure” in calligraphy is the "
"range of your hand's movement, and it is therefore convenient to have the "
"width of your pen nib in constant ratio to the size of your “drawing board” "
"and not in some real units which would make it depend on zoom. This behavior "
"is optional though, so it can be changed for those who would prefer absolute "
"units regardless of zoom. To switch to this mode, use the checkbox on the "
"tool's Preferences page (you can open it by double-clicking the tool button)."
msgstr ""
"Cette paire d'options contrôle la <firstterm>largeur</firstterm> de votre "
"plume. La largeur peut varier entre 1 et 100 et est (par défaut) mesurée "
"selon une unité relative à la taille de votre fenêtre d'édition, mais "
"indépendante du zoom. En effet, l'« unité de mesure » naturelle en "
"calligraphie est l'amplitude du mouvement de votre main et il est donc "
"pratique d'avoir votre largeur de plume en rapport constant par rapport à "
"votre « planche d'écriture » et non pas en unités réelles, ce qui la ferait "
"dépendre du zoom. Ce comportement est toutefois optionnel, et peut donc être "
"modifié pour ceux qui préféreraient une unité absolue peu importe le zoom. "
"Pour passer à ce mode, utilisez la case à cocher sur la page de Préférences "
"de l'outil (vous pouvez l'ouvrir en double-cliquant sur le bouton de "
"l'outil)."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:111
msgid ""
"Since pen width is changed often, you can adjust it without going to the "
"toolbar, using the <keycap>left</keycap> and <keycap>right</keycap> arrow "
"keys or with a tablet that supports the pressure sensitivity function. The "
"best thing about these keys is that they work while you are drawing, so you "
"can change the width of your pen gradually in the middle of the stroke:"
msgstr ""
"Comme la largeur de plume varie souvent, vous pouvez l'ajuster sans avoir à "
"retourner à la barre d'outils, en utilisant les touches fléchées "
"<keycap>gauche</keycap> et <keycap>droite</keycap> ou avec une tablette qui "
"supporte la sensibilité à la pression. L'avantage de ces raccourcis est "
"qu'ils fonctionnent même pendant que vous dessinez, de sorte que vous pouvez "
"faire varier la largeur de votre plume progressivement pendant un tracé :"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:124
msgid ""
"Pen width may also depend on the velocity, as controlled by the "
"<firstterm>thinning</firstterm> parameter. This parameter can take values "
"from -100 to 100; zero means the width is independent of velocity, positive "
"values make faster strokes thinner, negative values make faster strokes "
"broader. The default of 10 means moderate thinning of fast strokes. Here are "
"a few examples, all drawn with width=20 and angle=90:"
msgstr ""
"La largeur de la plume peut aussi dépendre de la vélocité, et ceci est "
"contrôlé par le paramètre d'<firstterm>amincissement</firstterm>. Ce "
"paramètre peut prendre une valeur entre -100 et 100 ; zéro signifie que la "
"largeur est indépendante de la vélocité, des valeurs positives font que des "
"tracés plus rapides sont plus fins, des valeurs négatives font que des "
"tracés plus rapides deviennent plus épais. La valeur par défaut de 10 "
"implique un amincissement modéré des tracés rapides. Voici quelques "
"exemples, tous tracés avec une largeur de 20 et un angle de 90° :"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:137
msgid ""
"For fun, set Width and Thinning both to 100 (maximum) and draw with jerky "
"movements to get strangely naturalistic, neuron-like shapes:"
msgstr ""
"Pour vous amuser, donnez une valeur de 100 (le maximum) à la largeur et à "
"l'amincissement puis dessinez avec des mouvements brusques pour obtenir des "
"formes étrangement naturalistes, ressemblant à des neurones :"

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:150
msgid "Angle &amp; Fixation"
msgstr "Angle et fixité"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:151
msgid ""
"After width, <firstterm>angle</firstterm> is the most important calligraphy "
"parameter. It is the angle of your pen in degrees, changing from 0 "
"(horizontal) to 90 (vertical counterclockwise) or to -90 (vertical "
"clockwise). Note that if you turn tilt sensitivity on for a tablet, the "
"angle parameter is greyed out and the angle is determined by the tilt of the "
"pen."
msgstr ""
"Après la largeur, l'<firstterm>angle</firstterm> est le paramètre "
"calligraphique le plus important. Il s'agit de l'angle de votre plume en "
"degrés, allant de 0° (horizontale) à 90° (verticale en sens anti-horaire) ou "
"-90° (verticale en sens horaire). Notez que si vous activez la sensibilité à "
"l'inclinaison sur votre tablette, le paramètre angle est grisé et l'angle "
"est déterminé par l'inclinaison de la plume."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:164
msgid ""
"Each traditional calligraphy style has its own prevalent pen angle. For "
"example, the Uncial hand uses the angle of 25 degrees. More complex hands "
"and more experienced calligraphers will often vary the angle while drawing, "
"and Inkscape makes this possible by pressing <keycap>up</keycap> and "
"<keycap>down</keycap> arrow keys or with a tablet that supports the tilt "
"sensitivity feature. For beginning calligraphy lessons, however, keeping the "
"angle constant will work best. Here are examples of strokes drawn at "
"different angles (fixation = 100):"
msgstr ""
"Chaque style de calligraphie traditionnelle possède son propre angle "
"prédominant. L'écriture onciale, par exemple, utilise un angle de 25 degrés. "
"Les styles plus complexes et les calligraphes plus expérimentés feront "
"souvent varier cet angle pendant le tracé, ce qu'Inkscape permet par "
"pression sur les touches fléchées <keycap>haut</keycap> et <keycap>bas</"
"keycap> ou avec une tablette qui supporte la sensibilité à l'inclinaison. "
"Néanmoins, pour l'exercice des débutants en calligraphie, conserver un angle "
"constant fonctionnera mieux. Voici des exemples de tracés dessinés selon "
"différents angles (fixité = 100) :"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:178
msgid ""
"As you can see, the stroke is at its thinnest when it is drawn parallel to "
"its angle, and at its broadest when drawn perpendicular. Positive angles are "
"the most natural and traditional for right-handed calligraphy."
msgstr ""
"Comme vous pouvez le voir, le tracé est plus fin quand il est parallèle à "
"l'angle, et plus épais quand il est perpendiculaire. Des angles positifs "
"sont plus naturels et traditionnels pour une calligraphie tracée de la main "
"droite."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:182
msgid ""
"The level of contrast between the thinnest and the thickest is controlled by "
"the <firstterm>fixation</firstterm> parameter. The value of 100 means that "
"the angle is always constant, as set in the Angle field. Decreasing fixation "
"lets the pen turn a little against the direction of the stroke. With "
"fixation=0, pen rotates freely to be always perpendicular to the stroke, and "
"Angle has no effect anymore:"
msgstr ""
"Le niveau de contraste entre l'épaisseur et la finesse maximales est "
"contrôlé par le paramètre <firstterm>fixité</firstterm>. Une valeur de 100 "
"signifie que l'angle est toujours constant, égal à la valeur définie dans le "
"champ angle. Diminuer la fixité permet à la plume de tourner légèrement dans "
"la direction opposée au tracé. Avec fixité=0, la plume tourne librement pour "
"être toujours perpendiculaire au tracé, et la valeur Angle n'a plus d'effet :"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:195
msgid ""
"Typographically speaking, maximum fixation and therefore maximum stroke "
"width contrast (above left) are the features of antique serif typefaces, "
"such as Times or Bodoni (because these typefaces were historically an "
"imitation of fixed-pen calligraphy). Zero fixation and zero width contrast "
"(above right), on the other hand, suggest modern sans serif typefaces such "
"as Helvetica."
msgstr ""
"Typographiquement parlant, une fixité maximale et donc un contraste maximal "
"de largeur de tracé (ci-dessus à gauche) sont les caractéristiques des "
"antiques fontes serif, comme Times ou Bodoni (parce que ces fontes étaient "
"historiquement une imitation d'une calligraphie effectuée avec une plume "
"orientée de façon fixe). D'un autre côté, une fixité nulle et donc un "
"contraste de largeur nulle (ci-dessus à droite) font plutôt penser à une "
"fonte sans serif moderne, comme Helvetica."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:203
msgid "Tremor"
msgstr "Tremblement"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:204
msgid ""
"<firstterm>Tremor</firstterm> is intended to give a more natural look to the "
"calligraphy strokes. Tremor is adjustable in the Controls bar with values "
"ranging from 0 to 100. It will affect your strokes producing anything from "
"slight unevenness to wild blotches and splotches. This significantly expands "
"the creative range of the tool."
msgstr ""
"Le <firstterm>tremblement</firstterm> est conçu pour donner une apparence "
"plus naturelle aux tracés calligraphiques. Le tremblement est ajustable dans "
"la barre de contrôle, avec des valeurs comprises entre 0 et 100. Ceci "
"influencera vos tracés, produisant différents effets pouvant aller des "
"légères inégalités aux bavures et tâches. Cela augmente significativement "
"les possibilités de l'outil."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:219
msgid "Wiggle &amp; Mass"
msgstr "Agitation et inertie"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:220
msgid ""
"Unlike width and angle, these two last parameters define how the tool "
"“feels” rather than affect its visual output. So there won't be any "
"illustrations in this section; instead just try them yourself to get a "
"better idea."
msgstr ""
"Contrairement à la largeur et à l'angle, ces deux derniers paramètres "
"définissent la « sensation » laissée par l'outil au lieu d'affecter son "
"rendu visuel. Il n'y aura donc pas d'illustration dans cette section ; à la "
"place, effectuez simplement des tests par vous-même pour vous faire une idée "
"du fonctionnement."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:225
msgid ""
"<firstterm>Wiggle</firstterm> is the resistance of the paper to the movement "
"of the pen. The default is at minimum (0), and increasing this parameter "
"makes paper “slippery”: if the mass is big, the pen tends to run away on "
"sharp turns; if the mass is zero, high wiggle makes the pen to wiggle wildly."
msgstr ""
"L'<firstterm>agitation</firstterm> est la résistance du papier au mouvement "
"de la plume. La valeur par défaut est au minimum (0), et augmenter ce "
"paramètre rend le papier glissant : si la masse est forte, la plume a "
"tendance à partir dans des changements de directions brusques ; si la masse "
"est nulle, une agitation forte provoquera des tortillements incontrôlables "
"de la plume."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:230
msgid ""
"In physics, <firstterm>mass</firstterm> is what causes inertia; the higher "
"the mass of the Inkscape calligraphy tool, the more it lags behind your "
"mouse pointer and the more it smoothes out sharp turns and quick jerks in "
"your stroke. By default this value is quite small (2) so that the tool is "
"fast and responsive, but you can increase mass to get slower and smoother "
"pen."
msgstr ""
"En physique, la <firstterm>masse</firstterm> est ce qui génère l'inertie ; "
"plus la masse de l'outil Calligraphie d'Inkscape est importante, et plus "
"celui-ci traîne derrière votre stylet (ou pointeur de souris) et lisse les "
"changements de direction et mouvements brusques de votre tracé. Par défaut, "
"cette valeur est assez faible (2) de façon à ce que l'outil soit rapide et "
"réactif, mais vous pouvez augmenter la masse pour obtenir une plume plus "
"lente et douce."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:238
msgid "Calligraphy examples"
msgstr "Exemples de calligraphies"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:239
msgid ""
"Now that you know the basic capabilities of the tool, you can try to produce "
"some real calligraphy. If you are new to this art, get yourself a good "
"calligraphy book and study it with Inkscape. This section will show you just "
"a few simple examples."
msgstr ""
"Maintenant que vous connaissez les possibilités de base de l'outil, vous "
"pouvez essayer de produire de vraies calligraphies. Si vous êtes débutant "
"dans cette discipline, procurez-vous un bon livre sur la calligraphie et "
"étudiez-le avec Inkscape. Cette section vous montrera juste quelques "
"exemples simples."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:244
msgid ""
"First of all, to do letters, you need to create a pair of rulers to guide "
"you. If you're going to write in a slanted or cursive hand, add some slanted "
"guides across the two rulers as well, for example:"
msgstr ""
"Tout d'abord pour tracer des lettres, vous avez besoin d'une paire de règles "
"pour vous guider. Si vous devez avoir une écriture inclinée ou cursive, "
"ajoutez aussi des guides inclinés en travers des deux règles horizontales. "
"Par exemple :"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:255
msgid ""
"Then zoom in so that the height between the rulers corresponds to your most "
"natural hand movement range, adjust width and angle, and off you go!"
msgstr ""
"Ensuite, le zoom devra être réglé de telle sorte que la hauteur entre les "
"règles corresponde à votre amplitude naturelle de mouvement de main. Ajustez "
"les paramètres largeur et angle, et c'est parti !"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:259
msgid ""
"Probably the first thing you would do as a beginner calligrapher is practice "
"the basic elements of letters — vertical and horizontal stems, round "
"strokes, slanted stems. Here are some letter elements for the Uncial hand:"
msgstr ""
"La première chose que vous devriez faire en tant que calligraphe débutant "
"est probablement de vous entraîner à dessiner les éléments de base des "
"lettres — des traits horizontaux et verticaux, des tracés ronds et des "
"traits inclinés. Voici quelques éléments de lettres pour l'écriture onciale :"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:271
msgid "Several useful tips:"
msgstr "Plusieurs astuces utiles :"

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:276
msgid ""
"If your hand is comfortable on the tablet, don't move it. Instead, scroll "
"the canvas (<keycombo><keycap function=\"control\">Ctrl</"
"keycap><keycap>arrow</keycap></keycombo> keys) with your left hand after "
"finishing each letter."
msgstr ""
"Si votre main est confortablement installée sur la tablette, ne la déplacez "
"pas. Faites plutôt défiler le canevas (touches <keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>flèches</keycap></keycombo>) avec votre "
"main gauche après avoir fini chaque lettre."

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:283
msgid ""
"If your last stroke is bad, just undo it (<keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>Z</keycap></keycombo>). However, if its "
"shape is good but the position or size are slightly off, it's better to "
"switch to Selector temporarily (<keycap>Space</keycap>) and nudge/scale/"
"rotate it as needed (using <mousebutton>mouse</mousebutton> or keys), then "
"press <keycap>Space</keycap> again to return to Calligraphy tool."
msgstr ""
"Si votre dernier coup de plume est mauvais, annulez-le (<keycombo><keycap "
"function=\"control\">Ctrl</keycap><keycap>Z</keycap></keycombo>). Cependant, "
"si sa forme est bonne, mais que la position et la taille doivent être "
"légèrement rectifiées, il vaut mieux utiliser le Sélecteur temporairement "
"(<keycap>espace</keycap>) et opérer les déplacement/redimensionnement/"
"rotation nécessaires (en utilisant la <mousebutton>souris</mousebutton> ou "
"le clavier), puis appuyer sur <keycap>espace</keycap> de nouveau pour "
"reprendre l'outil Calligraphie."

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:292
msgid ""
"Having done a word, switch to Selector again to adjust stem uniformity and "
"letterspacing. Don't overdo this, however; good calligraphy must retain "
"somewhat irregular handwritten look. Resist the temptation to copy over "
"letters and letter elements; each stroke must be original."
msgstr ""
"Quand vous avez fini un mot, passez de nouveau au Sélecteur pour ajuster "
"l'uniformité des traits et l'inter-lettrage. N'en faites pas trop "
"cependant ; une belle calligraphie doit garder le côté légèrement irrégulier "
"d'une écriture manuscrite. Résistez à la tentation de dupliquer des lettres "
"et des morceaux de lettres ; chaque coup de plume doit être original."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:299
msgid "And here are some complete lettering examples:"
msgstr "Et voici quelques exemples de lettrages complets :"

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:311
msgid "Conclusion"
msgstr "Conclusion"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:312
msgid ""
"Calligraphy is not only fun; it's a deeply spiritual art that may transform "
"your outlook on everything you do and see. Inkscape's calligraphy tool can "
"only serve as a modest introduction. And yet it is very nice to play with "
"and may be useful in real design. Enjoy!"
msgstr ""
"La calligraphie n'est pas seulement amusante ; c'est un art profondément "
"spirituel qui peut transformer votre regard sur tout ce que vous faites et "
"voyez. L'outil Calligraphie d'Inkscape ne peut servir que d'introduction "
"modeste. Et pourtant, il est très amusant de jouer avec et il peut servir à "
"des travaux concrets. Faites-vous plaisir !"

#. (itstool) path: Work/format
#: calligraphy-f01.svg:48 calligraphy-f02.svg:48 calligraphy-f03.svg:48
#: calligraphy-f04.svg:80 calligraphy-f05.svg:48 calligraphy-f06.svg:64
#: calligraphy-f07.svg:49 calligraphy-f08.svg:48 calligraphy-f09.svg:48
#: calligraphy-f10.svg:48
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: calligraphy-f01.svg:69
#, no-wrap
msgid "width=1, growing....                       reaching 47, decreasing...                                  back to 0"
msgstr "largeur=1, augmentant…                 atteignant 47, diminuant…                      de nouveau à 0"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:69
#, no-wrap
msgid "thinning = 0 (uniform width) "
msgstr "amincissement = 0 (largeur uniforme) "

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:80
#, no-wrap
msgid "thinning = 10"
msgstr "amincissement = 10"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:91
#, no-wrap
msgid "thinning = 40"
msgstr "amincissement = 40"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:102
#, no-wrap
msgid "thinning = −20"
msgstr "amincissement = −20"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:113
#, no-wrap
msgid "thinning = −60"
msgstr "amincissement = −60"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:120
#, no-wrap
msgid "angle = 90 deg"
msgstr "angle = 90°"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:131
#, no-wrap
msgid "angle = 30 (default)"
msgstr "angle = 30° (par défaut)"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:142 calligraphy-f05.svg:102
#, no-wrap
msgid "angle = 0"
msgstr "angle = 0°"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:153
#, no-wrap
msgid "angle = −90 deg"
msgstr "angle = −90°"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:69 calligraphy-f06.svg:85 calligraphy-f06.svg:101
#: calligraphy-f06.svg:117
#, no-wrap
msgid "angle = 30"
msgstr "angle = 30°"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:80
#, no-wrap
msgid "angle = 60"
msgstr "angle = 60°"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:91
#, no-wrap
msgid "angle = 90"
msgstr "angle = 90°"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:113
#, no-wrap
msgid "angle = 15"
msgstr "angle = 15°"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:124
#, no-wrap
msgid "angle = −45"
msgstr "angle = −45°"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:90
#, no-wrap
msgid "fixation = 100"
msgstr "fixité = 100"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:106
#, no-wrap
msgid "fixation = 80"
msgstr "fixité = 80"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:122
#, no-wrap
msgid "fixation = 0"
msgstr "fixité = 0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:73
#, no-wrap
msgid "slow"
msgstr "lent"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:84
#, no-wrap
msgid "medium"
msgstr "moyen"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:95
#, no-wrap
msgid "fast"
msgstr "rapide"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:179
#, no-wrap
msgid "tremor = 0"
msgstr "tremblement = 0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:190
#, no-wrap
msgid "tremor = 10"
msgstr "tremblement = 10"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:201
#, no-wrap
msgid "tremor = 30"
msgstr "tremblement = 30"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:212
#, no-wrap
msgid "tremor = 50"
msgstr "tremblement = 50"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:223
#, no-wrap
msgid "tremor = 70"
msgstr "tremblement = 70"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:234
#, no-wrap
msgid "tremor = 90"
msgstr "tremblement = 90"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:245
#, no-wrap
msgid "tremor = 20"
msgstr "tremblement = 20"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:256
#, no-wrap
msgid "tremor = 40"
msgstr "tremblement = 40"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:267
#, no-wrap
msgid "tremor = 60"
msgstr "tremblement = 60"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:278
#, no-wrap
msgid "tremor = 80"
msgstr "tremblement = 80"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:289
#, no-wrap
msgid "tremor = 100"
msgstr "tremblement = 100"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:117
#, no-wrap
msgid "Uncial hand"
msgstr "Écriture onciale"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:128
#, no-wrap
msgid "Carolingian hand"
msgstr "Écriture carolingienne"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:139
#, no-wrap
msgid "Gothic hand"
msgstr "Écriture gothique"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:150
#, no-wrap
msgid "Bâtarde hand"
msgstr "Écriture bâtarde"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:175
#, no-wrap
msgid "Flourished Italic hand"
msgstr "Écriture italique florissante"

#~ msgid "Western or Roman"
#~ msgstr "Occidentale ou romane"

#~ msgid "Arabic"
#~ msgstr "Arabe"

#~ msgid ""
#~ "This tutorial focuses mainly on Western calligraphy, as the other two "
#~ "styles tend to use a brush (instead of a pen with nib), which is not how "
#~ "our Calligraphy tool currently functions."
#~ msgstr ""
#~ "Ce didacticiel se focalise principalement sur la calligraphie "
#~ "occidentale, étant donné que les deux autres styles nécessitent "
#~ "généralement un pinceau (au lieu d'une plume), qui ne fait pas encore "
#~ "partie des fonctions de notre outil Calligraphie."
