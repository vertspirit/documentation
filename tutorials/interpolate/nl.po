# Dutch translations for Inkscape.
# This file is distributed under the same license as the Inkscape package.
#
# Kris De Gussem <kris.DeGussem@gmail.com>, 2010.
#
# *** Stuur een mailtje
# *** voordat je met dit bestand aan de slag gaat,
# *** om dubbel werk en stijlbreuken te voorkomen.
#
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Interpolate\n"
"POT-Creation-Date: 2021-03-28 20:01+0200\n"
"PO-Revision-Date: 2010-11-11 12:30+0100\n"
"Last-Translator: Kris De Gussem <Kris.DeGussem@gmail.com>\n"
"Language-Team: Dutch\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Dutch\n"
"X-Poedit-Country: NETHERLANDS\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "Kris De Gussem <kris.DeGussem@gmail.com>, 2010"

#. (itstool) path: articleinfo/title
#: tutorial-interpolate.xml:6
msgid "Interpolate"
msgstr "Interpoleren"

#. (itstool) path: articleinfo/subtitle
#: tutorial-interpolate.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-interpolate.xml:11
msgid "This document explains how to use Inkscape's Interpolate extension"
msgstr ""
"Dit document legt uit hoe je de Inkscape-uitbreiding Interpoleren gebruikt"

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:16
msgid "Introduction"
msgstr "Introductie"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:17
msgid ""
"Interpolate does a <firstterm>linear interpolation</firstterm> between two "
"or more selected paths. It basically means that it “fills in the gaps” "
"between the paths and transforms them according to the number of steps given."
msgstr ""
"Interpoleren maakt een <firstterm>lineaire interpolatie</firstterm> tussen "
"twee of meer geselecteerde paden. Eenvoudig gezegd betekent het dat het “de "
"gaten vult” tussen de paden en deze transformeert in overeenstemming met het "
"aantal gegeven stappen."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:22
#, fuzzy
msgid ""
"To use the Interpolate extension, select the paths that you wish to "
"transform, and choose <menuchoice><guimenu>Extensions</"
"guimenu><guisubmenu>Generate From Path</guisubmenu><guimenuitem>Interpolate</"
"guimenuitem></menuchoice> from the menu."
msgstr ""
"Selecteer voor het gebruik van Interpoleren de paden die je wil "
"transformeren en kies <command>Uitbreidingen &gt; Genereren uit pad &gt; "
"Interpoleren</command> in het menu."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:27
#, fuzzy
msgid ""
"Before invoking the extension, the objects that you are going to transform "
"need to be <emphasis>paths</emphasis>. This is done by selecting the object "
"and using <menuchoice><guimenu>Path</guimenu><guimenuitem>Object to Path</"
"guimenuitem></menuchoice> or <keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>C</keycap></"
"keycombo>. If your objects are not paths, the extension will do nothing."
msgstr ""
"Vooraleer de uitbreiding toe passen, moeten de objecten die je wil "
"transformeren, omgezet worden in <emphasis>paden</emphasis>. Dit kan door de "
"objecten te selecteren en <command>Paden &gt; Object naar pad</command> of "
"<keycap>Shift+Ctrl+C</keycap>te gebruiken. Indien je objecten geen paden "
"zijn, doet de uitbreiding niets."

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:36
msgid "Interpolation between two identical paths"
msgstr "Interpolatie tussen twee identieke paden"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:37
msgid ""
"The simplest use of the Interpolate extension is to interpolate between two "
"paths that are identical. When the extension is called, the result is that "
"the space between the two paths is filled with duplicates of the original "
"paths. The number of steps defines how many of these duplicates are placed."
msgstr ""
"Het eenvoudigste gebruik van de uitbreiding Interpoleren is interpoleren "
"tussen twee identieke paden. Wanneer deze aangeroepen wordt, is het "
"resultaat dat de ruimte tussen de twee paden opgevuld wordt met duplicaten "
"van de originele paden. Het aantal stappen bepaalt het aantal geplaatste "
"duplicaten."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:42 tutorial-interpolate.xml:75
msgid "For example, take the following two paths:"
msgstr "Neem bijvoorbeeld de volgende twee paden:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:52
msgid ""
"Now, select the two paths, and run the Interpolate extension with the "
"settings shown in the following image."
msgstr ""
"Selecteer nu beide paden en voer Interpoleren uit met de instellingen zoals "
"getoond in de volgende afbeelding."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:62
msgid ""
"As can be seen from the above result, the space between the two circle-"
"shaped paths has been filled with 6 (the number of interpolation steps) "
"other circle-shaped paths. Also note that the extension groups these shapes "
"together."
msgstr ""
"Zoals je kan zien in het bovenstaande resultaat, is de ruimte tussen de twee "
"cirkelvormige paden gevuld met zes (het aantal interpolatiestappen) andere "
"cirkelvormige paden. Noteer ook dat Interpoleren deze vormen groepeert."

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:69
msgid "Interpolation between two different paths"
msgstr "Interpolatie tussen twee verschillende paden"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:70
msgid ""
"When interpolation is done on two different paths, the program interpolates "
"the shape of the path from one into the other. The result is that you get a "
"morphing sequence between the paths, with the regularity still defined by "
"the Interpolation Steps value."
msgstr ""
"Wanneer interpolatie toegepast wordt op twee verschillende paden "
"interpoleert het programma de vorm van het ene pad in het andere. Het "
"resultaat is dat je een morphingsequentie tussen de twee paden krijgt, "
"waarbij de regelmatigheid bepaald wordt door het aantal interpolatiestappen."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:85
msgid ""
"Now, select the two paths, and run the Interpolate extension. The result "
"should be like this:"
msgstr ""
"Selecteer nu de twee paden en pas Interpoleren toe. Het resultaat zou "
"ongeveer zo moeten zijn:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:95
msgid ""
"As can be seen from the above result, the space between the circle-shaped "
"path and the triangle-shaped path has been filled with 6 paths that progress "
"in shape from one path to the other."
msgstr ""
"Zoals je kan zien in het bovenstaande resultaat, is de ruimte tussen het "
"cirkelvormige pad en het driehoekpad opgevuld met zes paden die gradueel van "
"de ene vorm in de andere overgaan."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:99
msgid ""
"When using the Interpolate extension on two different paths, the "
"<emphasis>position</emphasis> of the starting node of each path is "
"important. To find the starting node of a path, select the path, then choose "
"the Node Tool so that the nodes appear and press <keycap>TAB</keycap>. The "
"first node that is selected is the starting node of that path."
msgstr ""
"Bij het gebruik van de uitbreiding Interpoleren op twee verschillende paden, "
"is de <emphasis>positie</emphasis> van het beginknooppunt van elk pad van "
"belang. Om het beginknooppunt te vinden, selecteer je het pad en schakel je "
"over naar het knooppuntengereedschap opdat de knooppunten verschijnen. Druk "
"op <keycap>TAB</keycap>. Het eerste geselecteerde knooppunt is het "
"beginknooppunt van het pad."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:105
msgid ""
"See the image below, which is identical to the previous example, apart from "
"the node points being displayed. The node that is green on each path is the "
"starting node."
msgstr ""
"Bekijk onderstaande afbeelding die identiek is aan het vorige voorbeeld, "
"behalve dat de knooppunten worden weergegeven. Het groene knooppunt op elk "
"pad is het beginknooppunt."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:116
msgid ""
"The previous example (shown again below) was done with these nodes being the "
"starting node."
msgstr ""
"Het vorige voorbeeld (hier nogmaals weergegeven) werd toegepast met deze "
"knooppunten als de beginknooppunten."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:126
msgid ""
"Now, notice the changes in the interpolation result when the triangle path "
"is mirrored so the starting node is in a different position:"
msgstr ""
"Bekijk nu de veranderingen in het interpolatieresultaat wanneer de driehoek "
"gespiegeld wordt opdat het startknooppunt zich in een andere positie bevindt:"

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:146
msgid "Interpolation Method"
msgstr "Interpolatiemethode"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:147
msgid ""
"One of the parameters of the Interpolate extension is the Interpolation "
"Method. There are 2 interpolation methods implemented, and they differ in "
"the way that they calculate the curves of the new shapes. The choices are "
"either Interpolation Method 1 or 2."
msgstr ""
"Een van de parameters van de Interpoleren is de Interpolatiemethode. Er zijn "
"twee interpolatiemethoden geïmplementeerd die verschillen in de wijze waarop "
"de curven van de nieuwe paden berekend worden. De mogelijke keuzen zijn "
"Interpolatiemethode 1 of 2."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:152
#, fuzzy
msgid ""
"In the examples above, we used Interpolation Method 2, and the result was:"
msgstr ""
"In bovenstaande voorbeelden gebruikten we Interpolatiemethode 2 en was het "
"resultaat:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:162
#, fuzzy
msgid "Now compare this to Interpolation Method 1:"
msgstr "Vergelijk dit met Interpolatiemethode 1:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:172
msgid ""
"The differences in how these methods calculate the numbers is beyond the "
"scope of this document, so simply try both, and use which ever one gives the "
"result closest to what you intend."
msgstr ""
"De verschillen on hoe deze methoden de getallen berekenen valt buiten scope "
"van dit document. Probeer dus gewoon beide en gebruik het resultaat dat het "
"dichtste aanleunt bij wat je in gedachten had."

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:178
msgid "Exponent"
msgstr "Exponent"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:179
msgid ""
"The <firstterm>exponent parameter</firstterm> controls the spacing between "
"steps of the interpolation. An exponent of 0 makes the spacing between the "
"copies all even."
msgstr ""
"De <firstterm>parameter exponent</firstterm> bepaalt de afstand tussen de "
"stappen van de interpolatie. Een exponent 0 zorgt voor een gelijke afstand "
"tussen de kopieën"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:183
msgid "Here is the result of another basic example with an exponent of 0."
msgstr "Hier is het resultaat van nog een eenvoudig voorbeeld met exponent 0."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:193
#, fuzzy
msgid "The same example with an exponent of 1:"
msgstr "Hetzelfde voorbeeld met exponent 1:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:203
#, fuzzy
msgid "with an exponent of 2:"
msgstr "met exponent 2:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:213
#, fuzzy
msgid "and with an exponent of -1:"
msgstr "met exponent -1:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:223
msgid ""
"When dealing with exponents in the Interpolate extension, the "
"<emphasis>order</emphasis> that you select the objects is important. In the "
"examples above, the star-shaped path on the left was selected first, and the "
"hexagon-shaped path on the right was selected second."
msgstr ""
"Wanneer je exponenten gebruikt bij Interpoleren, is de <emphasis>volgorde</"
"emphasis> waarin je de objecten selecteert van belang. In de bovenstaande "
"voorbeelden is het stervormige pad links eerst geselecteerd en het "
"hexagonale pad rechts als tweede."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:228
#, fuzzy
msgid ""
"View the result when the path on the right was selected first. The exponent "
"in this example was set to 1:"
msgstr ""
"Bekijk het resultaat wanneer het rechtse pad eerst was geselecteerd. De "
"exponent in dit voorbeeld is 1:"

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:240
msgid "Duplicate Endpaths"
msgstr "Eindpaden dupliceren"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:241
msgid ""
"This parameter defines whether the group of paths that is generated by the "
"extension <emphasis>includes a copy</emphasis> of the original paths that "
"interpolate was applied on."
msgstr ""
"Deze parameter bepaalt of de groep paden die door de uitbreiding gegenereert "
"wordt, <emphasis>een kopie bevat</emphasis> van de originele paden waarop "
"Interpoleren toegepast werd."

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:247
msgid "Interpolate Style"
msgstr "Stijl interpoleren"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:248
msgid ""
"This parameter is one of the neat functions of the interpolate extension. It "
"tells the extension to attempt to change the style of the paths at each "
"step. So if the start and end paths are different colors, the paths that are "
"generated will incrementally change as well."
msgstr ""
"Deze parameter is een van de mooie functies van Interpoleren. Het zorgt "
"ervoor dat Interpoleren bij elke stap de stijl van de paden tracht te "
"veranderen. Bijgevolg, indien het begin- en eindpad verschillende kleuren "
"hebben, zullen de gegeneerde paden incrementeel van kleur veranderen."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:253
msgid ""
"Here is an example where the Interpolate Style function is used on the fill "
"of a path:"
msgstr ""
"Dit is een voorbeeld waarbij Stijl interpoleren gebruikt is op de vulling "
"van een pad."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:263
msgid "Interpolate Style also affects the stroke of a path:"
msgstr "Stijl interpoleren beïnvloedt ook de omlijning van een pad:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:273
msgid ""
"Of course, the path of the start point and the end point does not have to be "
"the same either:"
msgstr "Begin- en eindpad hoeven uiteraard niet hetzelfde te zijn:"

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:285
msgid "Using Interpolate to fake irregular-shaped gradients"
msgstr "Gebruik voor imitatie van onregelmatige kleurverlopen"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:286
#, fuzzy
msgid ""
"At the time when gradient meshes were not implemented in Inkscape, it was "
"not possible to create a gradient other than linear (straight line) or "
"radial (round). However, it could be faked using the Interpolate extension "
"and Interpolate Style. A simple example follows — draw two lines of "
"different strokes:"
msgstr ""
"Het is in Inkscape (nog) niet mogelijk om andere dan linaire (rechte lijn) "
"of randiale (ronde) kleurverlopen te maken. Dit kan echter geïmiteerd worden "
"door Interpoleren met Stijl interpoleren. Zie het volgende eenvoudige "
"voorbeeld - teken twee lijnen met verschillende omlijning:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:298
msgid "And interpolate between the two lines to create your gradient:"
msgstr "En interpoleer tussen de twee lijnen om je kleurverloop te maken:"

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:310
msgid "Conclusion"
msgstr "Conclusie"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:311
msgid ""
"As demonstrated above, the Inkscape Interpolate extension is a powerful "
"tool. This tutorial covers the basics of this extension, but experimentation "
"is the key to exploring interpolation further."
msgstr ""
"Zoals hierboven gedemonstreerd, is de Inskcapeuitbreiding Interpoleren een "
"krachtige tool. Deze handleiding omvat de basis van de uitbreiding, maar "
"proberen is de weg naar het verder exploreren van interpolatie."

#. (itstool) path: Work/format
#: interpolate-f01.svg:49 interpolate-f02.svg:49 interpolate-f03.svg:49
#: interpolate-f04.svg:49 interpolate-f05.svg:49 interpolate-f07.svg:49
#: interpolate-f08.svg:49 interpolate-f09.svg:49 interpolate-f10.svg:49
#: interpolate-f11.svg:49 interpolate-f12.svg:49 interpolate-f13.svg:49
#: interpolate-f14.svg:49 interpolate-f15.svg:49 interpolate-f16.svg:49
#: interpolate-f17.svg:49 interpolate-f18.svg:49 interpolate-f19.svg:49
#: interpolate-f20.svg:52
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:126 interpolate-f04.svg:117 interpolate-f11.svg:117
#, no-wrap
msgid "Exponent: 0.0"
msgstr "Exponent: 0.0"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:130 interpolate-f04.svg:121 interpolate-f11.svg:121
#, no-wrap
msgid "Interpolation Steps: 6"
msgstr "Interpolatiestappen: 6"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:134 interpolate-f04.svg:125 interpolate-f11.svg:125
#, no-wrap
msgid "Interpolation Method: 2"
msgstr "Interpolatiemethode: 2"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:138 interpolate-f04.svg:129 interpolate-f11.svg:129
#, no-wrap
msgid "Duplicate Endpaths: unchecked"
msgstr "Eindpaden dupliceren: uitgevinkt"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:142 interpolate-f04.svg:133 interpolate-f11.svg:133
#, no-wrap
msgid "Interpolate Style: unchecked"
msgstr "Stijl interpoleren: uitgevinkt"

#, fuzzy
#~ msgid "ryanlerch at gmail dot com"
#~ msgstr "Ryan Lerch, ryanlerch at gmail dot com"
